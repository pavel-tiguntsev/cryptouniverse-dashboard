import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueCookies from 'vue-cookies'
import VueMoment from 'vue-moment';
import VModal from 'vue-js-modal/dist/ssr.index'
import VueFilter from 'vue-filter';
import VueChartkick from 'vue-chartkick';
import Highcharts from 'highcharts';



export default () => {
  Vue.use(VueAxios, axios)
  Vue.use(VueCookies);
  Vue.use(VueMoment);
  Vue.use(VModal);
  Vue.use(VueFilter);
  Vue.use(VueChartkick, {adapter: Highcharts})

}
