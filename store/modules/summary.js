import axios from 'axios'
import api from '~/config'

const state = {
  summaryFetched: false,
  summary: {},
  summaryPlot: [],
  summaryPlotRealtime: [],
}

const getters = {
  summaryFetched: state => state.summaryFetched || false,
  summary: state => state.summary,
  summaryPlotRealtime: state => state.summaryPlotRealtime,
  summaryByRange: state => name => (
    state.summaryPlot.find(miner => miner.name == name)
    ? state.summaryPlot.find(miner => miner.name == name).plot
    : []
  ),
  summarySumHR: state => name => {
    let hr = state.summary[name].sumHR;
    return convertToDeep(hr, getDeep(hr))
  },
  summarySumHRUnit: state => name => {
    let deep = getDeep(state.summary[name].sumHR);
    return getUnit(deep);
  },
  summaryCount: state => name => state.summary[name].number,
  summaryUnit: state => name => (
    state.summaryPlot.find(miner => miner.name == name)
    ? state.summaryPlot.find(miner => miner.name == name).unit
    : 'h/s'
  ),
}

const actions = {
  getSummary: ({commit, rootState}) => {
    axios.get(api.url + '/getMain?token=' + rootState.token).then((res) => {
      commit('summaryFetcheded')
      commit('setSummary', { summary: res.data.summary })
      commit('addSummary', { summary: res.data.summary })
    })
  },
  getSummaryByRange: ({commit, rootState}, {type, range}) => {
    axios.get(api.url + '/userhrs?type=' + range +'&token=' + rootState.token).then((res) => {
      commit('setSummaryByRange', { data: res.data, type: type })
    })
  }
}

const mutations = {
  summaryFetcheded: (state) => {
    state.summaryFetched = true;
  },
  setSummary: (state, { summary }) => {
    state.summary = summary;
  },
  addSummary: (state, { summary }) => {
    Object.keys(summary).forEach((key) => {
        var obj = state.summaryPlotRealtime.find(miner => miner.name == key);
        if (summary[key].number != 0) {
          if (obj == undefined) {
            state.summaryPlotRealtime.push(
              { name: key, deep: getDeep(summary[key].sumHR), unit: getUnit(getDeep(summary[key].sumHR)), plot: [{x: new Date(), y: convertToDeep(summary[key].sumHR, getDeep(summary[key].sumHR))}] }
            )
          } else {
            obj.plot.push({ x: new Date(), y: convertToDeep(summary[key].sumHR, obj.deep) });
          }
        }
    })
  },
  setSummaryByRange: (state, {data, type}) => {
    Object.keys(data).forEach((key) => {
      var obj = state.summaryPlot.find(miner => miner.name == key);
      var out = []
      data[key].forEach((row, index) => {
        out.push({ x: new Date(row.date * 1000), y: row.value })
      });
      if (obj == undefined) {
        state.summaryPlot.push(
          { name: key, plot: out }
        )
      } else {
        obj.plot = out;
      }
      obj = state.summaryPlot.find(miner => miner.name == key);

      // TODO remove when fixed backend
      obj.plot.pop();
      setDeep(obj);
    })
  }
}

const setDeep = (obj) => {
  let minDeep = 10;
  obj.plot.forEach((row, index) => {
    if (minDeep > getDeep(row.y))
      minDeep = getDeep(row.y);
  });
  obj.unit = getUnit(minDeep)
  for (const row of obj.plot) {
    row.y = convertToDeep(row.y, minDeep);
  }
}

const getDeep = (value) => {
  let deep = 0;
  while (value > 1000) {
    value = value / 1000;
    deep++;
  }
  return deep;
}

const convertToDeep = (value, deep) => {
  for (let index = 0; index < deep; index++) {
    value = value / 1000;
  }
  return value;
}

const getUnit = (deep) => {
  switch (deep) {
    case 1:
      return 'kH/s'
    case 2:
      return 'MH/s'
    case 3:
      return 'GH/s'
    case 4:
      return 'TH/s'
    case 5:
      return 'PH/s'
    case 6:
      return 'EH/s'
    default:
      return 'H/s';
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
