const state = {
  firstName: '',
  lastName: '',
  avatarURL: 'https://pp.userapi.com/c844520/v844520072/3d31/KI1b1KfFrZ0.jpg',
  email: '',
  bitcoinWallet: '',
  litecoinWallet: '',
};

const getters = {
  firstName: state => state.firstName,
  lastName: state => state.lastName,
  fullName: state => state.firstName + ' ' + state.lastName,
  avatarURL: state => state.avatarURL,
  email: state => state.email,
  bitcoinWallet: state => state.bitcoinWallet,
  litecoinWallet: state => state.litecoinWallet,
};

const actions = {
  updateEmail: ({ commit }, value) => { commit('setEmail', { value: value }) },
  updateBitcoin: ({ commit }, value) => { commit('setBitcoin', { value: value }) },
  updateLitecoin: ({ commit }, value) => { commit('setLitecoin', { value: value }) }
};

const mutations = {
  setEmail: (state, { value }) => {
    state.email = value;
  },
  setBitcoin: (state, { value }) => {
    state.bitcoinWallet = value;
  },
  setLitecoin: (state, { value }) => {
    state.litecoinWallet = value;
  },
};

export default {
  state,
  getters,
  actions,
  mutations
}
