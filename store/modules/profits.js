import axios from 'axios'
import api from '~/config'

const state = {
  S9: [],
  L3: [],
  summary: {}
};

const getters = {
  profits: state => state,
  balance: (state, getters) => name => name === "S9" ? getters.balanceBTC : getters.balanceLTC,
  balanceBTC: state => state.S9.reduce((sum, miner) => sum + miner.profit, 0),
  balanceLTC: state => state.L3.reduce((sum, miner) => sum + miner.profit, 0)
};

const actions = {
  getProfit: ({commit, state, rootState}) => {
    axios.get(api.url + '/getMyProfit?token=' + rootState.token).then((res) => {
      commit('setProfit', {profits: res.data})
    })
  },
};

const mutations = {
  setProfit: (state, { profits }) => {
    Object.assign(state, profits)
  },
};

export default {
  state,
  getters,
  actions,
  mutations
}
