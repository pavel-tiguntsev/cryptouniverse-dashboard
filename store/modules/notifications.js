const state = [
  // {id: 4, text: 'Добро пожаловать!', read: false, time: new Date()},
  // {id: 3, text: 'Добро пожаловать!', read: false, time: new Date()},
  // {id: 2, text: 'Добро пожаловать!', read: false, time: new Date()},
  {id: 1, text: 'Добро пожаловать!', read: true, time: new Date()},
];


const getters = {
  notifications: state => state,
  notificationsCount: state => state.filter(notify => notify.read === false).length
};

const actions = {
  readNotifications: ({commit}) => {
    commit('setNotificationsReaded');
  }
};

const mutations = {
  setNotificationsReaded: (state) => {
    state.forEach(notify => {
      notify.read = true
    });
  }
};

export default {
  state,
  getters,
  actions,
  mutations
}
