import axios from 'axios'
import api from '~/config'

const state = {
  minersFetched: false,
  miners: [],
}

const getters = {
  minersFetched: state => state.minersFetched,
  miners: state => state.miners,
}

const actions = {
  getMiners: ({commit, rootState}) => {
    axios.get(api.url + '/getMain?token=' + rootState.token).then((res) => {
      commit('minersFetched')
      commit('setMiners', { miners: res.data.miners })
    })
  }
}

const mutations = {
  minersFetched: (state) => {
    state.minersFetched = true;
  },
  setMiners: (state, { miners }) => {
    state.miners = miners;
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
