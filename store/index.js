import Vue from 'vue'
import Vuex from 'vuex'
import miners from './modules/miners'
import summary from './modules/summary'
import user from './modules/user'
import notifications from './modules/notifications'
import profits from './modules/profits'
import * as Cookies from 'js-cookie'
import axios from 'axios'
import api from '~/config'

Vue.use(Vuex)
const store = () => new Vuex.Store({
  modules: {
    miners,
    user,
    notifications,
    summary,
    profits
  },
  state: {
    login: 'no',
    user: {},
    token: '',
    rates: '',
    predicts: {BTC: [], LTC: []}
  },
  actions: {
    getPredicts: ({commit, state}) => {
      axios.get(api.url + '/getpredicts?token=' + state.token).then((res) => {
        commit('setPredicts', {predicts: res.data})
      })
    },
    getRates: ({commit, state}) => {
      axios.get(api.url + '/getrates').then((res) => {
        commit('setRates', {rates: res.data})
      })
    },
    getUser: ({commit, state}) => {
      axios.get(api.url + '/user?token=' + state.token).then((res) => {
        commit('login', {user: res.data.user})
      })
    },
    logout: ({commit}) => {
      commit('removeToken')
    }
  },
  mutations: {
    setPredicts: (state, { predicts }) => {
      state.predicts = predicts;
    },
    setRates: (state, { rates }) => {
      state.rates = rates;
    },
    login (state, {user}) {
      state.user = user;
      state.login = 'yes'
    },
    setToken: (state, token) => {
      state.token = token;
    },
    removeToken: (state) => {
      state.token = ""
      state.user = {}
    }
  }
})

export default store;
