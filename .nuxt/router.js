import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const _56580ff2 = () => import('../pages/users.vue' /* webpackChunkName: "pages/users" */).then(m => m.default || m)
const _935f9432 = () => import('../pages/tech.vue' /* webpackChunkName: "pages/tech" */).then(m => m.default || m)
const _19743eb7 = () => import('../pages/payout.vue' /* webpackChunkName: "pages/payout" */).then(m => m.default || m)
const _5911c0e8 = () => import('../pages/login.vue' /* webpackChunkName: "pages/login" */).then(m => m.default || m)
const _0cb2c8a0 = () => import('../pages/home.vue' /* webpackChunkName: "pages/home" */).then(m => m.default || m)
const _51e383e5 = () => import('../pages/miners.vue' /* webpackChunkName: "pages/miners" */).then(m => m.default || m)
const _5cf49c74 = () => import('../pages/settings.vue' /* webpackChunkName: "pages/settings" */).then(m => m.default || m)
const _5b724c3a = () => import('../pages/test.vue' /* webpackChunkName: "pages/test" */).then(m => m.default || m)
const _e12bc45e = () => import('../pages/index.vue' /* webpackChunkName: "pages/index" */).then(m => m.default || m)



if (process.client) {
  window.history.scrollRestoration = 'manual'
}
const scrollBehavior = function (to, from, savedPosition) {
  // if the returned position is falsy or an empty object,
  // will retain current scroll position.
  let position = false

  // if no children detected
  if (to.matched.length < 2) {
    // scroll to the top of the page
    position = { x: 0, y: 0 }
  } else if (to.matched.some((r) => r.components.default.options.scrollToTop)) {
    // if one of the children has scrollToTop option set to true
    position = { x: 0, y: 0 }
  }

  // savedPosition is only available for popstate navigations (back button)
  if (savedPosition) {
    position = savedPosition
  }

  return new Promise(resolve => {
    // wait for the out transition to complete (if necessary)
    window.$nuxt.$once('triggerScroll', () => {
      // coords will be used if no selector is provided,
      // or if the selector didn't match any element.
      if (to.hash) {
        let hash = to.hash
        // CSS.escape() is not supported with IE and Edge.
        if (typeof window.CSS !== 'undefined' && typeof window.CSS.escape !== 'undefined') {
          hash = '#' + window.CSS.escape(hash.substr(1))
        }
        try {
          if (document.querySelector(hash)) {
            // scroll to anchor by returning the selector
            position = { selector: hash }
          }
        } catch (e) {
          console.warn('Failed to save scroll position. Please add CSS.escape() polyfill (https://github.com/mathiasbynens/CSS.escape).')
        }
      }
      resolve(position)
    })
  })
}


export function createRouter () {
  return new Router({
    mode: 'history',
    base: '/',
    linkActiveClass: 'nuxt-link-active',
    linkExactActiveClass: 'nuxt-link-exact-active',
    scrollBehavior,
    routes: [
		{
			path: "/users",
			component: _56580ff2,
			name: "users"
		},
		{
			path: "/tech",
			component: _935f9432,
			name: "tech"
		},
		{
			path: "/payout",
			component: _19743eb7,
			name: "payout"
		},
		{
			path: "/login",
			component: _5911c0e8,
			name: "login"
		},
		{
			path: "/home",
			component: _0cb2c8a0,
			name: "home"
		},
		{
			path: "/miners",
			component: _51e383e5,
			name: "miners"
		},
		{
			path: "/settings",
			component: _5cf49c74,
			name: "settings"
		},
		{
			path: "/test",
			component: _5b724c3a,
			name: "test"
		},
		{
			path: "/",
			component: _e12bc45e,
			name: "index"
		}
    ],
    
    
    fallback: false
  })
}
